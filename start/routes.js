'use strict'


/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in Adopets API' }
})

//Route.post('/login', 'UserController.login').middleware('guest')


  Route.post('/register', 'UserController.register')
  Route.post('/login', 'UserController.login')

  Route.get('/user/:id', 'UserController.show')
  Route.get('/logout', 'UserController.logout')

  Route.get('/products/:page?', 'ProductController.index')
  Route.post('/products', 'ProductController.store')
  Route.put('/product/:id', 'ProductController.update')
  Route.delete('/product/:id', 'ProductController.delete')

  Route.get('/search', 'SearchProductController.index')








