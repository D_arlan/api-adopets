"use strict";

const uuidv4 = require("uuid/v4");

const ProductHook = (exports = module.exports = {});

ProductHook.uuid = async product => {
  product.id = uuidv4();
};