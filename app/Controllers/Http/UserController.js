'use strict'

const User = use('App/Models/User')

class UserController {



    async register({request, res, auth}){
        const data = await request.all()
        const user = await User.create(data)
        await auth.login(user)
        return user
    }

    async login ({ request, auth }) {
        const { email, password } = request.all()
        const result = await auth.attempt(email, password)
        return result
    }

    async logout({ auth, response, session }){

        await auth.logout();
        session.flash({ notification: "Logged out successfully" });

        //return response.redirect("/");
    }

    
    
    

    async show ({ auth, params }) {
        if (auth.user.id !== params.id){
          return 'You cannot see someone else\'s profile'
        }
        return auth.user
      }

    
}

module.exports = UserController
