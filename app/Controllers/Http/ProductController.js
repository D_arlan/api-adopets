'use strict'

const { post } = require("@adonisjs/framework/src/Route/Manager")

const Products = use('App/Models/Products')

class ProductController {

    async index({request, response, params, session}){
        const id_user = session.all()

        //console.log(id_user['adonis-auth']);
        const user_id = request.header('authorization')
        
        

        const page = params.page ||1
        const products = await Products.query().where('user_id', user_id).paginate(page, 10)
        return products
    }


    async store({request, response}){
        const data = await request.all()
        const product = await Products.create(data)
        return product
    }
    async delete({request, response, params}){


        const { id } = request.params;
        console.log(id);
        const product = await Products.find(id)
        
        await product.delete()

        //console.log(product);
        
        response.status(200).send({message:'Product deleted!'})

    }
    
    async update({request, response, params}){
        const data = await request.all()

       

        const product = await Products.find(params.id)

        if(!product){
            response.status(404).send({message:'Product not found!'})
        }

        product.name = data.name
        product.description = data.description
        product.category = data.category
        product.price = data.price
        product.stock = data.stock
        

        await product.save()
        const all = request.all()

        return product
    }
}

module.exports = ProductController
