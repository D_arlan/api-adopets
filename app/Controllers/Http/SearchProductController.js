'use strict'

const { post } = require("@adonisjs/framework/src/Route/Manager")

const Products = use('App/Models/Products')

class ProductController {

    async index({request, response}){
        const query = request.get()
        //console.log(query);
        

        const queryProducts = Products
        .query()
        //.with('products')
        
            queryProducts.where('name', 'like', `%${query.name}%`)
            .orWhere('category', 'like', `%${query.category}%`)
            .orWhere('description', 'like', `%${query.description}%`)
        
        
       
        const products = await queryProducts.paginate(1, 10)
        return products
    }
}

module.exports = ProductController
